const express = require("express");
const router = express.Router();


router.get('/examen',(req,res)=>{
    const valores= {
            
        recibo:req.query.recibo,
        tipoServicio:req.query.tipoServicio,
        edad:req.query.edad,
        domicilio:req.query.domicilio,
        nombre:req.query.nombre,
        consumo:req.query.consumo,
        costo:req.query.costo,
        descuento:req.query.descuento,
        impuesto:req.query.impuesto,
        subtotal:req.query.subtotal,
        pagoTotal:req.query.pagoTotal
         }
    res.render('formulario.html',valores);
})
router.get('/resultados',(req,res)=>{
    const valores= {
            
        recibo:req.query.recibo,
        tipoServicio:req.query.tipoServicio,
        edad:req.query.edad,
        domicilio:req.query.domicilio,
        nombre:req.query.nombre,
        consumo:req.query.consumo,
        costo:req.query.costo,
        descuento:req.query.descuento,
        impuesto:req.query.impuesto,
        subtotal:req.query.subtotal,
        pagoTotal:req.query.pagoTotal
         }
    res.render('resultados.html',valores);
})
  

    router.post('/examen',(req,res)=>{
        const valores= {
            
            recibo:req.body.recibo,
            tipoServicio:req.body.tipoServicio,
            domicilio:req.body.domicilio,
            nombre:req.body.nombre,
            consumo:req.body.consumo,
            costo:req.body.costo,
            descuento:req.body.descuento,
            impuesto:req.body.impuesto,
            subtotal:req.query.subtotal,
            pagoTotal:req.body.pagoTotal
             }
        res.render('formulario.html',valores);
    })
    router.post('/resultados',(req,res)=>{
        const valores= {
            
            recibo:req.body.recibo,
            tipoServicio:req.body.tipoServicio,
            domicilio:req.body.domicilio,
            nombre:req.body.nombre,
            consumo:req.body.consumo,
            costo:req.body.costo,
            descuento:req.body.descuento,
            impuesto:req.body.impuesto,
            subtotal:req.query.subtotal,
            pagoTotal:req.body.pagoTotal
             }
        res.render('resultados.html',valores);
    })
    module.exports=router;